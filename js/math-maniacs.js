// START APP
$(document).ready(function(){
  $('#math-maniacs-wrapper .start-btn').on('click', function(){
    $(this).prop('disabled', true);
    datetime = Math.round((new Date()).getTime() / 1000) +10;
    console.log(datetime);
    $('#flipdown').empty();
    var flipdown = new FlipDown(datetime);
    flipdown.start();
    flipdown.ifEnded(() => {
      $('#math-maniacs-wrapper .start-btn').prop('disabled', false);
    });
      
  });

});