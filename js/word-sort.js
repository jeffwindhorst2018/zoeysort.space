// Check the sorted list
function checkLists() {
  answer=getListText('#unsorted-list ul li').sort();
  response=getListText('#sorted-list ul li');
  if(JSON.stringify(answer) == JSON.stringify(response)) {
    $('#results-div').addClass('success');
    $('#results-div').html('<span class="green">CORRECT!</span');
  } else {
    $('#results-div').addClass('incorrect');
    $('#results-div').html('<span class="red">INCORRECT. Click Reset to try again.</span');
  }
  $('#results-div').show();
}

function getListText(list) {
  arr=$(list).map(function(){
    return $.trim($(this).text());
  });
  return arr;
}

function alreadyChosen(w) {
  found=false;
  arr=getListText('#sorted-list ul li');
  for(i=0; i<arr.length; i++) {
    if(w == arr[i]) {
      found=true;
    }
  }
  return found;
}

// START APP
$(document).ready(function(){
  
  $('#total-words').text(words.length);
  shuffled=words;
  shuffle(words);

  // Show Instructions
  $('#instructions').on('click', function(){
    $('#instructions-block').show();
  });

  // Hide Instrucations
  $('#instructions-block h2 a').on('click',  function(){
    $('#instructions-block').hide();
  });

  for(i=0; i<words.length; i++) {
    $("#all-words").append(words[i]+' ');
  }

  // Show list
  $('#show-list').on('click', function(){
      $("#all-words").show();
  });

  // Close list
  $('#close-all-words').on('click', function(){
    $('#all-words').hide();
  });

  // Select word count
  $('#word-count').on('change', function(){
    if($(this).val() != '') {
      maxNum=$(this).val();
      $('#unsorted-list ul li').remove();
      $('#sorted-list ul li').remove();
      chosenWords=getRandom(words, maxNum);
      $('#unsorted-list ul li').remove();
      for(i=0; i<chosenWords.length; i++) {
        $('#unsorted-list ul').append('<li data-word="'+chosenWords[i]+'">' + chosenWords[i] + '</li>');
      }
    }
  });

  // Add to sorted list
  $('body #unsorted-list').on('click', 'li', function(){
    if(!alreadyChosen($(this).text())) {
      $(this).addClass('strike');
      $('#sorted-list ul').append('<li>' + $(this).text() + '</li>');
      if($('#sorted-list ul li').length == $('#unsorted-list ul li').length) {
        checkLists();
      }
    }
  });

  // Remove from sorted list
  $('body #sorted-list').on('click', 'li', function() {
    text=$(this).text();
    $('#unsorted-list ul').find("[data-word='"+text+"'").removeClass('strike');
    $(this).remove();
  });

  // Reset sorted list
  $('#reset-sorted').on('click', function(){
    $('#word-count').val('');
    $('#results-div').hide();
    $('#results-div').removeClass('success, incorrect');
    $('#sorted-list ul li').remove();
    $('#unsorted-list ul li').remove();
    $('#unsorted-list ul li').removeClass('strike');
    $('#results-div span').remove();
  });

  function getRandom(arr, n) {
    var result = new Array(n),
      len = arr.length,
      taken = new Array(len);
    if (n > len)
      throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
  }

  function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
});