 // 
      // ATTENTION: To change the wall words; replace, add, or remove words from the list below.
      // The last word should NOT have a comma after it.
      // Do NOT change anything else in this file unless you know what you are doing.
      ///////////////////////////////////////////////////////////////////////////////////////////////////////
var words=[
  'a',
  'all',
  'am',
  'an',
  'an',
  'and',
  'are',
  'as',
  'ask',
  'at',
  'back',
  'be',
  'best',
  'big',
  'black',
  'blue',
  'bring',
  'brown',
  'but',
  'by',
  'call',
  'came',
  'can',
  'come',
  'could',
  'day',
  'did',
  'down',
  'each',
  'eat',
  'fall',
  'fast',
  'find',
  'five',
  'for',
  'friend',
  'from',
  'full',
  'gave',
  'get',
  'go',
  'good',
  'got',
  'had',
  'has',
  'have',
  'he',
  'help',
  'her',
  'here',
  'him',
  'his',
  'hot',
  'how',
  'I',
  'if',
  'in',
  'is',
  'it',
  'jump',
  'let',
  'like',
  'little',
  'look',
  'man',
  'may',
  'me',
  'my',
  'next',
  'new',
  'no',
  'not',
  'now',
  'of',
  'off',
  'on',
  'one',
  'out',
  'place',
  'play',
  'pull',
  'rain',
  'ran',
  'red',
  'ride',
  'run',
  'said',
  'same',
  'saw',
  'say',
  'see',
  'she',
  'sing',
  'sit',
  'small',
  'some',
  'take',
  'tell',
  'than',
  'that',
  'the',
  'them',
  'then',
  'there',
  'they',
  'three',
  'to',
  'us',
  'use',
  'want',
  'was',
  'we',
  'well',
  'went',
  'were',
  'what',
  'when',
  'where',
  'white',
  'who',
  'why',
  'will',
  'with',
  'write',
  'yes',
  'you',
  'your'
];
//
// END OF WORD LIST
///////////////////////////////////////////////////////////////////////////////////////////////////////